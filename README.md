This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
=======

## Overview

Create a simple app to display the weather in three cities.
Implement a fully functional React app from the mockup. Approximate spacing, responsiveness, colors etc. as closely as possible.
Use the API of any free weather service to show weather in real time for three cities of your choosing. (yahoo, openweathermap, theweathernetwork, etc).
All weather should be represented by an icon or graphic as shown in the mockup. You may choose to use any icon pack you like, such as Fontawesome.
Deploy to a server that can be accessed over the internet. You can use any host of your choosing such as AWS, Google Cloud, Heroku, Azure etc. Be sure to include the url in your submission.
Requirements
The app is built with React
The app is fully functional and all weather is retrieved in real time The app is properly deployed to a hosting service
All UI elements respond appropriately to user actions
Code is hosted in a public repository, like Github.com
Bonus: Use of typescript
Bonus: Use of LESS in part or completely
Bonus: Use of React class components instead of functional components
What we're looking for
1. Accurate implementation of mockup
2. High quality and polished code
3. Use of modern tooling, libraries and ES6+ functions