import React, { Component } from "react";
import ReactDOM from "react-dom";
import moment from 'moment';
import updateRA from 'immutability-helper';

import '../../styles/styles.less';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSnowflake, faCloud, faCloudRain, faCloudShowersHeavy, faSun, faBoltLightning, faWind, faCircleExclamation } from '@fortawesome/free-solid-svg-icons'

const CITY_OPTIONS = [
  {
    cityName: 'Ottawa',
    lat: 45.4215,
    lon: -75.6972,
  },
  {
    cityName: 'Newmarket',
    lat: 44.0501,
    lon: -79.4663,
  },
  {
    cityName: 'Vancouver',
    lat: 49.2497,
    lon: -123.1193,
  },
]

const getIcon = function getIcon(weather) {
  if (weather === 'Clouds') {
    return <FontAwesomeIcon icon={faCloud} />
  } else if (weather === 'Snow') {
    return <FontAwesomeIcon icon={faSnowflake} />
  } else if (weather === 'Drizzle') {
    return <FontAwesomeIcon icon={faCloudRain} />
  } else if (weather === 'Rain') {
    return <FontAwesomeIcon icon={faCloudShowersHeavy} />
  } else if (weather === 'Clear') {
    return <FontAwesomeIcon icon={faSun} />
  } else if (weather === 'Thunderstorm') {
    return <FontAwesomeIcon icon={faBoltLightning} />
  } else if (weather === 'Mist' || weather === 'Smoke' || weather === 'Haze' || weather === 'Dust' || weather === 'Fog' || weather === 'Sand' || weather === 'Ash' || weather === 'Squall' || weather === 'Tornado') {
    return <FontAwesomeIcon icon={faWind} />
  } else {
    return <FontAwesomeIcon icon={faCircleExclamation} />
  }
}

class WeatherApp extends Component {

  constructor() {
    super();

    this.state = {
      currentCity: 'Ottawa',
      currentLat: 45.4215,
      currentLon: -75.6972,
      todaysData: [],
      forecastData: [],
      error: null,
      isLoaded: false,
    };
    this.getData = this.getData.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  getData() {
    const { currentLat, currentLon } = this.state;

    let $ = this;
    fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${currentLat}&lon=${currentLon}&units=metric&cnt=5&appid=82eca22c1424fb49706bb438a02493da`)
      .then(function (response) {
        return response.json();
      }).then(function (result) {
        const { daily, current } = result;
        daily.shift();
        const dailyForecast = daily.splice(0, 4);
        $.setState({
          todaysData: current,
          forecastData: dailyForecast,
          isLoaded: true,
        });
      }).catch(function (error) {
        $.setState({
          isLoaded: true,
          error: error
        });
      });
  }

  onChange(newCityName) {
    const { currentCity, currentLat, currentLon } = this.state;
    let newCityNameValue = currentCity;
    let newCurrentLatValue = currentLat;
    let newCurrentLonValue = currentLon;

    

    switch (newCityName) {
      case 'Ottawa':
        var result = CITY_OPTIONS.filter(obj => {
          return obj.cityName === newCityName
        })
        newCityNameValue = newCityName;
        newCurrentLatValue = result[0].lat;
        newCurrentLonValue = result[0].lon;
        break;
      case 'Newmarket':
        var result = CITY_OPTIONS.filter(obj => {
          return obj.cityName === newCityName
        })
        newCityNameValue = newCityName;
        newCurrentLatValue = result[0].lat;
        newCurrentLonValue = result[0].lon;
        break;
      case 'Vancouver':
        var result = CITY_OPTIONS.filter(obj => {
          return obj.cityName === newCityName
        })
        newCityNameValue = newCityName;
        newCurrentLatValue = result[0].lat;
        newCurrentLonValue = result[0].lon;
        break;
    }

    let newState = updateRA(this.state, {
      'currentCity': { $set: newCityNameValue },
      'currentLat': { $set: newCurrentLatValue },
      'currentLon': { $set: newCurrentLonValue },
      'isLoaded': {$set: false},
    });
    this.setState(newState, () => this.getData());
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    const { error, isLoaded, forecastData, todaysData, currentCity } = this.state;
    const { temp: todaysTemp, weather } = todaysData;
    const todaysWeather = weather;

    if (error) {
      return <div>Error</div>;
    } else if (!isLoaded) {
      return (
        <>
          <div class="buttonWrapper">
            {CITY_OPTIONS.map((item, i) => (
              <button type="button" onClick={() => {this.onChange(item.cityName)}} className={item.cityName === currentCity ? 'active' : ''} >{item.cityName}</button>
            ))}
          </div>
          <div className="weatherWrapper">
            <div className="todaysForecastContainer">
              <div className="todayWeatherInnerDetails">
                <h4>Loading...</h4>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div class="buttonWrapper">
            {CITY_OPTIONS.map((item, i) => (
              <button type="button" onClick={() => {this.onChange(item.cityName)}} className={item.cityName === currentCity ? 'active' : ''}>{item.cityName}</button>
            ))}
          </div>
          <div className="weatherWrapper">
            <div className="todaysForecastContainer">
              <h3>Today</h3>
              <div className="todayWeatherInnerDetails">
                <div className="detailsIcon">
                  {getIcon(todaysWeather[0].main)}
                </div>
                <div className="detailsInfo">
                  <h4>{parseInt(todaysTemp)}°</h4>
                  <h5>{todaysWeather[0].main}</h5>

                </div>
              </div>

            </div>
            <div className="dailyForecastContainer">
              {forecastData.map((item, i) => (
                <div className="dailyForecastInner">
                  <h3>{moment.unix(item.dt).format('ddd')}</h3>
                  <div className="dailyForecastIcon">
                    {getIcon(item.weather[0].main)}
                  </div>
                  <h4>{parseInt(item.temp.day)}°</h4>
                </div>
              ))}
            </div>
          </div>
        </>
      );
    }
  }
}

export default WeatherApp;

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<WeatherApp />, wrapper) : false;